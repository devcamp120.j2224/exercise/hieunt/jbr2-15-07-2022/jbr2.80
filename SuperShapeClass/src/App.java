public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println("Shape 1 là: " + shape1);
        System.out.println("Shape 2 là: " + shape2);
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "green", true);
        System.out.println("Circle 1 là: " + circle1);
        System.out.println("Circle 2 là: " + circle2);
        System.out.println("Circle 3 là: " + circle3);
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle(2.0, 1.5, "green", true);
        System.out.println("Rectangle 1 là: " + rectangle1);
        System.out.println("Rectangle 2 là: " + rectangle2);
        System.out.println("Rectangle 3 là: " + rectangle3);
        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square(2.0, "green", true);
        System.out.println("Square 1 là: " + square1);
        System.out.println("Square 2 là: " + square2);
        System.out.println("Square 3 là: " + square3); 
        System.out.println("Circle 1 có diện tích là: " + circle1.getArea() + ", chu vi là: " + circle1.getPerimeter());
        System.out.println("Circle 2 có diện tích là: " + circle2.getArea() + ", chu vi là: " + circle2.getPerimeter());
        System.out.println("Circle 3 có diện tích là: " + circle3.getArea() + ", chu vi là: " + circle3.getPerimeter());
        System.out.println("Rectangle 1 có diện tích là: " + rectangle1.getArea() + ", chu vi là: " + rectangle1.getPerimeter());
        System.out.println("Rectangle 2 có diện tích là: " + rectangle2.getArea() + ", chu vi là: " + rectangle2.getPerimeter());
        System.out.println("Rectangle 3 có diện tích là: " + rectangle3.getArea() + ", chu vi là: " + rectangle3.getPerimeter());
        System.out.println("Square 1 có diện tích là: " + square1.getArea() + ", chu vi là: " + square1.getPerimeter());
        System.out.println("Square 2 có diện tích là: " + square2.getArea() + ", chu vi là: " + square2.getPerimeter());
        System.out.println("Square 3 có diện tích là: " + square3.getArea() + ", chu vi là: " + square3.getPerimeter());
    }
}
